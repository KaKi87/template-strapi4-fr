module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET'),
    events: {
      onConnectionSuccess: async event => {
        if(!event.user.preferedLanguage){
          await strapi.entityService.update(
              'admin::user',
              event.user.id,
              {
                data: {
                  'preferedLanguage': 'fr'
                }
              }
          );
        }
      }
    }
  },
  apiToken: {
    salt: env('API_TOKEN_SALT'),
  },
  transfer: {
    token: {
      salt: env('TRANSFER_TOKEN_SALT'),
    },
  },
  watchIgnoreFiles: [
    '**/config/sync/**',
  ],
  flags: {
    nps: false
  }
});
