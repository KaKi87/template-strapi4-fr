# `template-strapi4-fr`

## Getting started

```bash
git clone https://git.kaki87.net/KaKi87/template-strapi4-fr my-app
cd my-app
cp .env.example .env
cp src/admin/extensions/+config.example.js src/admin/extensions/+config.js
yarn install
yarn import-config -y
yarn start
```

Default credentials : `admin@localhost.local`/`admin`

[Strapi open office hours : Monday - Friday 11:00-12:00 & 19:30-20:30 CE(S)T](https://github.com/strapi/community-content/issues/1237)