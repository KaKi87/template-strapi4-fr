'use strict';

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register(/*{ strapi }*/) {},

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  async bootstrap(/*{ strapi }*/) {
    if(!await strapi.entityService.findOne('admin::user', 1)){
      await strapi.entityService.create(
        'admin::user',
        {
          data: {
            email: 'admin@localhost.local',
            firstname: 'admin',
            isActive: true,
            preferedLanguage: 'fr',
            roles: [1]
          }
        }
      );
      await strapi
        .query('admin::user')
        .update({
          where: {
            id: 1
          },
          data: {
            'password': '$2a$10$Dx4mGSuDEGsZeelg4UkHieZW7X9AE50cAiU16Ghj4cmdzz/2wVYyS'
          }
        });
    }
  }
};
