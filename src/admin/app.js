import {
  authTitle,
  authSubtitle
} from './extensions/+config.js';
import favicon from './extensions/favicon.png';

export default {
  config: {
    locales: ['fr'],
    translations: {
      'fr': {
        'Settings.apiTokens.title': `Jetons d'API`,
        'Settings.permissions.users.roles': `Rôles`,
        'Settings.permissions.users.username': `Nom d'utilisateur`,
        'Settings.permissions.users.user-status': `Statut`,
        'Settings.permissions.users.active': `Actif`,
        'Settings.permissions.users.inactive': `Inactif`,
        'Auth.form.welcome.title': authTitle,
        'Auth.form.welcome.subtitle': authSubtitle,
        'Auth.form.register.subtitle': authSubtitle
      },
      'en': {
        'Auth.form.welcome.title': authTitle,
        'Auth.form.welcome.subtitle': authSubtitle,
        'Auth.form.register.subtitle': authSubtitle
      }
    },
    auth: {
      logo: favicon
    },
    menu: {
      logo: favicon
    },
    head: {
      favicon
    }
  }
};